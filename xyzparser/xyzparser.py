#!/usr/bin/python3

import sys
import math


def readxyz(xyzname):
    """Read xyz file and return a list of cooridnate tuples.
    The first coordinate tuple (list index 0) is empty.
    """
    coords = list()
    coords.append(tuple())
    with open(xyzname, 'r') as fxyz:
        natoms = int(fxyz.readline().split()[0])
        fxyz.readline()
        for a in range(natoms):
            content = fxyz.readline().split()
            coords.append(tuple([float(x) for x in content[1:4]]))
    return coords


def norm(r1):
    """Return the norm of a vector.
    """
    assert len(r1) == 3
    return math.sqrt(r1[0]**2 + r1[1]**2 + r1[2]**2)


def dist(r1, r2):
    """Return the distance between two atoms.
    """
    assert len(r1) == 3
    assert len(r2) == 3
    dx = r1[0] - r2[0]
    dy = r1[1] - r2[1]
    dz = r1[2] - r2[2]
    return math.sqrt(dx**2 + dy**2 + dz**2)


def midpoint(r1, r2):
    """Return the midpoint between two atoms.
    """
    assert len(r1) == 3
    assert len(r2) == 3
    x = 0.5 * (r1[0] + r2[0])
    y = 0.5 * (r1[1] + r2[1])
    z = 0.5 * (r1[2] + r2[2])
    return (x, y, z)


def cross(r1, r2):
    """Return the cross product between two vectors.
    """
    assert len(r1) == 3
    assert len(r2) == 3
    x = r1[1] * r2[2] - r1[2] * r2[1]
    y = r1[2] * r2[0] - r1[0] * r2[2]
    z = r1[0] * r2[1] - r1[1] * r2[0]
    return (x, y, z)


def normvec(r1, r2, r3):
    """Return the normal vector (unnormalized) of the plane defined by three
    points.
    """
    assert len(r1) == 3
    assert len(r2) == 3
    assert len(r3) == 3
    r21 = (r1[0] - r2[0], r1[1] - r2[1], r1[2] - r2[2])
    r23 = (r3[0] - r2[0], r3[1] - r2[1], r3[2] - r2[2])
    return cross(r21, r23)


def angle(r1, r2):
    """Return the angle (in degree) between two vectors.
    """
    assert len(r1) == 3
    assert len(r2) == 3
    dotprod = r1[0] * r2[0] + r1[1] * r2[1] + r1[2] * r2[2]
    cosine = dotprod / (norm(r1) * norm(r2))
    return math.acos(cosine) / math.pi * 180.0


def acuteangle(r1, r2):
    """Return the acute angle (in degree) between two vectors.
    """
    assert len(r1) == 3
    assert len(r2) == 3
    dotprod = r1[0] * r2[0] + r1[1] * r2[1] + r1[2] * r2[2]
    cosine = dotprod / (norm(r1) * norm(r2))
    return math.acos(abs(cosine)) / math.pi * 180.0
