#!/usr/bin/env python3

from eckart_frame import read_mass, read_xyz, write_xyz, rotate_coords

import numpy as np
import math
import unittest


class TestEckartFrame(unittest.TestCase):

    def rmsd_mol(self, name):

        masses = read_mass("mass_{:s}.dat".format(name))
        ref_elems, ref_coords = read_xyz("ref_{:s}.xyz".format(name))
        inp_elems, inp_coords = read_xyz("inp_{:s}.xyz".format(name))
        self.assertEqual(ref_elems, inp_elems)

        out_coords = rotate_coords(ref_coords, inp_coords, masses)
        write_xyz("out_{:s}.xyz".format(name), ref_elems, out_coords)

        natoms = len(masses)
        diff_norm = np.linalg.norm(ref_coords - out_coords)
        rmsd = math.sqrt(diff_norm**2 / float(natoms))

        return rmsd

    def test_h2o(self):

        self.assertAlmostEqual(self.rmsd_mol("h2o"), 0.119183, 6)

    def test_spl(self):

        self.assertAlmostEqual(self.rmsd_mol("spl"), 1.316407, 6)


if __name__ == "__main__":
    unittest.main()
