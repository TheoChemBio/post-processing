import numpy as np
import math


def read_mass(massfile):
    """Reads masses from file"""

    masses = []
    with open(massfile, "r") as f_mass:
        for line in f_mass:
            masses += [float(x) for x in line.split()]
    return np.array(masses)


def read_xyz(xyzfile):
    """Reads coordinates from xyz file"""

    elems = []
    coords = []

    with open(xyzfile, "r") as f_xyz:
        natoms = int(f_xyz.readline().split()[0])
        f_xyz.readline()
        for a in range(natoms):
            content = f_xyz.readline().split()
            elems.append(content[0])
            coords += [float(x) for x in content[1:4]]

    return elems, np.array(coords).reshape(natoms, 3)


def write_xyz(xyzfile, elems, coords):
    """Write coordinates to xyz file"""

    natoms = len(elems)
    assert coords.shape == (natoms, 3)

    with open(xyzfile, "w") as f_xyz:
        f_xyz.write("{:d}\n\n".format(natoms))
        for a in range(natoms):
            f_xyz.write("{:s} {:f} {:f} {:f}\n".format(
                elems[a], coords[a, 0], coords[a, 1], coords[a, 2]))


def rotate_coords(ref_coords, inp_coords, masses):
    """Rotate inp_coords to Eckart frame defined by ref_coords"""

    # sanity check

    natoms = ref_coords.shape[0]
    assert ref_coords.shape == (natoms, 3)
    assert inp_coords.shape == (natoms, 3)
    assert masses.shape == (natoms,)

    # center of mass for ref_coords and inp_coords

    ref_com = np.array([np.dot(masses, ref_coords[:, d]) for d in range(3)])
    inp_com = np.array([np.dot(masses, inp_coords[:, d]) for d in range(3)])

    ref_com /= np.sum(masses)
    inp_com /= np.sum(masses)

    ref_com = np.vstack((ref_com,) * natoms)
    inp_com = np.vstack((inp_com,) * natoms)

    # center ref_coords and inp_coords

    ref_coords -= ref_com
    inp_coords -= inp_com

    # compute f matrix (Eckart frame)

    B = np.matmul(np.transpose(ref_coords), inp_coords)
    BB = np.matmul(B, np.transpose(B))

    # check planarity

    skip_dim = -1
    for d in range(3):
        if np.linalg.norm(BB[d, :]) < 1.0e-6:
            skip_dim = d

    # planar molecule

    if skip_dim in range(3):

        F = np.delete(BB, skip_dim, 0)
        F = np.delete(F, skip_dim, 1)
        eigval, eigvec = np.linalg.eig(F)

        diag = np.diag([1.0 / math.sqrt(eigval[i]) for i in range(2)])
        Delta_inv = np.matmul(eigvec, np.matmul(diag, np.transpose(eigvec)))

        ft = np.matmul(Delta_inv, np.delete(B, skip_dim, 0))
        ft2 = np.cross(ft[0], ft[1])
        ft = np.insert(ft, skip_dim, 0, axis=0)
        ft[skip_dim] = ft2[:]
        f = np.transpose(ft)

    # nonplanar molecule

    else:

        eigval, eigvec = np.linalg.eig(BB)

        diag = np.diag([1.0 / math.sqrt(eigval[i]) for i in range(3)])
        Delta_inv = np.matmul(eigvec, np.matmul(diag, np.transpose(eigvec)))

        ft = np.matmul(Delta_inv, B)
        f = np.transpose(ft)

    # double check that f*ft == unit matrix

    ff = np.matmul(f, ft)
    assert np.max(np.abs(ff - np.diag([1.0, 1.0, 1.0]))) < 1.0e-6

    # rotate inp_coords to Eckart frame defined by ref_coords

    out_coords = np.matmul(inp_coords, f)

    # translate back to respective center of mass

    out_coords += ref_com
    ref_coords += ref_com
    inp_coords += inp_com

    return out_coords
