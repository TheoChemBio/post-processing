#!/usr/bin/perl

if (@ARGV==0) {
   printf "Usage: perl gaussian_to_xyz.pl [gaussian_output_file]\n";
   exit;
}

$file = $ARGV[0];

## Define Atoms ##
@atom = qw/* H He Li Be  B  C  N  O  F Ne   Na Mg Al Si  P  S Cl Ar  K Ca
            Sc Ti  V Cr Mn Fe Co Ni Cu Zn   Ga Ge As Se Br Kr Rb Sr  Y Zr
            Nb Mo Tc Ru Rh Pd Ag Cd In Sn   Sb Te  I Xe Cs Ba La Ce Pr Nd
            Pm Sm Eu Gd Tb Dy Ho Er Tm Yb   Lu Hf Ta  W Re Os Ir Pt Au Hg
            Tl Pb Bi Po At Rn Fr Ra  /;

## Read Gaussian Output ##
$i = 0;
open GEOM,"$file.out" or die;
while(<GEOM>){
  $i++;
  if(/l9999/){
    $line = $i;
  }
}
close GEOM;

open GEOM,"$file.out" or die;
for $i ( 1 .. $line-1 ) {
  $_ = <GEOM>;
}
while(<GEOM>){
  if(/l9999/){
    printf "coordinates found in $file.out\n";
    while(<GEOM>){
      chomp;
      $line .= $_;
    }
  }
}
close GEOM;

## Write to GJF File ##
$line =~ s/ //g;
$line =~ s/\\\\/\n/g;
$coord = (split "\n",$line)[3];
$coord =~ s/,0,/,/g;
$coord =~ s/,/ /g;
$coord =~ s/\\/\n/g;
open GJF,">$file.gjf" or die;
print GJF "\#\n\n$file\n\n$coord\n\n\n";
close GJF;
printf "coordinates written to $file.gjf\n";

## Write to XYZ File ##
@xyz = split "\n",$coord;
$n = @xyz - 1;
open FL,">$file.xyz" or die;
print FL "$n\n\n";
for $i (1..$n) { printf FL "$xyz[$i]\n"; }
close FL;
printf "coordinates written to $file.xyz\n";
