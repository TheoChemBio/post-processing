#!/usr/bin/perl

die unless @ARGV==1;
$file = $ARGV[0];

@atom = qw/* H He Li Be  B  C  N  O  F Ne   Na Mg Al Si  P  S Cl Ar  K Ca
            Sc Ti  V Cr Mn Fe Co Ni Cu Zn   Ga Ge As Se Br Kr Rb Sr  Y Zr
            Nb Mo Tc Ru Rh Pd Ag Cd In Sn   Sb Te  I Xe Cs Ba La Ce Pr Nd
            Pm Sm Eu Gd Tb Dy Ho Er Tm Yb   Lu Hf Ta  W Re Os Ir Pt Au Hg
            Tl Pb Bi Po At Rn Fr Ra  /;

open GEOM,"$file.out" or die;
while(<GEOM>){
  if(/NAtoms\s*=\s+(\d+)\s+/){ $N_atom = $1; }
  if(/Standard orientation:/){
    $_ = <GEOM>; $_ = <GEOM>; $_ = <GEOM>; $_ = <GEOM>;
    for $i (1..$N_atom) {
      $_ = <GEOM>;
      $n[$i] = (split)[1];
      $x[$i] = (split)[3];
      $y[$i] = (split)[4];
      $z[$i] = (split)[5];
    }
  }
}
close GEOM;

open OUTPUT,">$file.xyz" or die;
print OUTPUT "$N_atom\n\n";
for $i (1..$N_atom) {
  printf OUTPUT "%s %s %s %s\n",$atom[$n[$i]],$x[$i],$y[$i],$z[$i];
}
close OUTPUT;
