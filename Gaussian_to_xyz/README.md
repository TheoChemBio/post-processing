## Gaussian_to_xyz

Convert Gaussian output files to xyz format.

+ Usage:

    ```
    bash run_xyz.sh
    ```
