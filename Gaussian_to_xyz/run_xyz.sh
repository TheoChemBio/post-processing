#!/bin/bash

set -u
set -e

# file extension for output

ext=out

# process all output files

files=$(ls *.$ext)

for file in $files; do

    name=${file%.$ext}
    echo $name

    perl gaussian_std_xyz.pl $name

done
